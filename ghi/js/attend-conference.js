window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingTag = document.getElementById("loading-conference-spinner");

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        console.log(response)
        for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
        }
        selectTag.classList.remove("d-none")
        loadingTag.classList.add("d-none")
    }
});
    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener("submit", async event =>{
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)


    const attendeeUrl='http://localhost:8001/attendees/';
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const attendeeResponse = await fetch(attendeeUrl, fetchConfig);
    const successAlert = document.getElementById('success-message')
    if (attendeeResponse.ok) {
        formTag.reset();
        formTag.classList.add("d-none")
        successAlert.classList.remove("d-none")
        const newAttendee = await attendeeResponse.json();
        console.log(newAttendee);
        }
    }
    )
