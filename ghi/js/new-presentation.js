window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    const selectTag = document.getElementById('conference');
    if (response.ok){
        const data = await response.json();

        for (let conference of data.conferences){
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option)
        }
    }

    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener("submit", async event =>{
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json)

    //const conference_id= selectTag.options[selectTag.selectedIndex].value
    const conferenceId= document.getElementById('conference')
    const value=conferenceId.value
    console.log(value)
    const presentationUrl=`http://localhost:8000${value}presentations/`
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const presentationResponse = await fetch(presentationUrl, fetchConfig);
    if (presentationResponse.ok) {
        formTag.reset();
        const newPresentation = await presentationResponse.json();
        console.log(newPresentation);
        }
    }
)
});
